module IntegrationTestHelper
  class Report
    attr_reader :report

    def initialize(raw)
      @report = JSON.parse(raw, symbolize_names: true)
    end

    def [](key)
      report[key]
    end

    def dependency_names
      report[:dependencies].map { |x| x[:name] }
    end

    def licenses_for(name)
      find(name)[:licenses]
    end

    def find(name)
      report[:dependencies].find do |dependency|
        dependency[:name] == name
      end
    end

    def nil?
      report.nil?
    end

    def to_hash
      to_h
    end

    def to_h
      report
    end
  end

  class IntegrationTestRunner
    attr_reader :project_path

    def initialize(project_path = File.join(Dir.pwd, 'tmp', SecureRandom.uuid))
      FileUtils.mkdir_p(project_path)
      @project_path = project_path
    end

    def add_file(name, content = nil)
      full_path = Pathname.new(File.join(project_path, name))
      FileUtils.mkdir_p(full_path.dirname)
      IO.write(full_path, block_given? ? yield : content)
    end

    def mount(dir:)
      FileUtils.cp_r("#{dir}/.", project_path)
    end

    def clone(repo, branch: 'master')
      if branch.match?(/\b[0-9a-f]{5,40}\b/)
        execute({}, 'git', 'clone', '--quiet', repo, project_path)
        Dir.chdir project_path do
          execute({}, 'git', 'checkout', branch)
        end
      else
        execute({}, 'git', 'clone', '--quiet', '--depth=1', '--single-branch', '--branch', branch, repo, project_path)
      end
    end

    def scan(env: {})
      return {} unless execute(env, './bin/docker-test', project_path)

      report_path = "#{project_path}/gl-license-management-report.json"
      return {} unless File.exist?(report_path)

      Report.new(IO.read(report_path))
    end

    def execute(env = {}, *args)
      Bundler.with_unbundled_env do
        system(env, *args, exception: true)
      end
    end

    def cleanup
      FileUtils.rm_rf(project_path) if Dir.exist?(project_path)
    end
  end

  def private_npm_host
    @private_npm_host ||= ENV.fetch('PRIVATE_NPM_HOST').tap do |host|
      add_host(host, ENV.fetch('PRIVATE_NPM_IP'))
    end
  end

  def private_pypi_host
    @private_pypi_host ||= ENV.fetch('PRIVATE_PYPI_HOST').tap do |host|
      add_host(host, ENV.fetch('PRIVATE_PYPI_IP'))
    end
  end

  def private_maven_host
    @private_maven_host ||= ENV.fetch('PRIVATE_MAVEN_HOST').tap do |host|
      add_host(host, ENV.fetch('PRIVATE_MAVEN_IP'))
    end
  end

  def runner(*args)
    @runner ||= IntegrationTestRunner.new(*args)
  end

  def add_host(name, ip)
    return unless ENV['LM_HOME']
    return if system("grep #{name} /etc/hosts")

    system("echo '#{ip} #{name}' >> /etc/hosts")
  end
end
