require 'simplecov'

require 'license/management'
require 'json'
require 'securerandom'
require 'json-schema'
require 'support/fixture_file_helper'
require 'support/integration_test_helper'
require 'support/matchers'
require 'support/shared'

RSpec.configure do |config|
  config.include FixtureFileHelper
  config.include IntegrationTestHelper, type: :integration
  config.define_derived_metadata(file_path: %r{/spec/integration}) do |metadata|
    metadata[:type] = :integration
  end
  config.after(:example, type: :integration) do
    runner.cleanup
  end
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end
  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end
  config.shared_context_metadata_behavior = :apply_to_host_groups
  config.filter_run_when_matching :focus
  config.disable_monkey_patching!
  config.warnings = true
  config.order = :random
  Kernel.srand config.seed
end
