# frozen_string_literal: true

module License
  module Management
    VERSION = '3.9.2'
  end
end
