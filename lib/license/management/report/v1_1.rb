# frozen_string_literal: true

module License
  module Management
    module Report
      class V1_1 < V1
        def to_h
          { version: '1.1' }.merge(super)
        end

        private

        def map_from_dependency(dependency)
          licenses = dependency.licenses.sort_by(&:name).map do |license|
            item = license_data(license)
            {
              name: item['name'],
              url: item.fetch('url', '')
            }
          end

          { licenses: licenses }.merge(super)
        end
      end
    end
  end
end
