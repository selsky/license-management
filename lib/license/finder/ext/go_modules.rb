# frozen_string_literal: true

module LicenseFinder
  class GoModules
    def prepare
      shell.execute([:go, :env])
      shell.execute([:go, :mod, :tidy, '-v', '&&', :go, :mod, :vendor, '-v'])
    end

    def active?
      Dir[project_path.join('go.sum')].any?
    end

    def current_packages
      stdout, _stderr, status = shell.execute(go_list_command)
      return [] unless status.success?

      stdout.each_line.map { |line| map_from(line) }.compact
    end

    private

    def go_list_command
      [:go, :list, '-m', '-f', "'{{.Path}},{{.Version}},{{.Dir}}'", :all]
    end

    def absolute_project_path
      @absolute_project_path ||= Pathname(project_path).cleanpath
    end

    def map_from(line)
      name, version, dir = line.chomp.split(',')
      return if dir.nil?
      return if Pathname(dir).cleanpath == absolute_project_path

      Package.new(name, version, install_path: dir)
    end
  end
end
