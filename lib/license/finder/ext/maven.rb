# frozen_string_literal: true

module LicenseFinder
  Maven.class_eval do
    XML_PARSE_OPTIONS = {
      'ForceArray' => %w[license dependency],
      'GroupTags' => {
        'licenses' => 'license',
        'dependencies' => 'dependency'
      }
    }.freeze

    def current_packages
      Dir.chdir(project_path) do
        LicenseFinder::SharedHelpers::Cmd.run(detect_licenses_command)
        resource_files.flat_map { |file| map_from(file.read) }.uniq
      end
    end

    private

    def detect_licenses_command
      [
        package_management_command,
        "-e",
        "org.codehaus.mojo:license-maven-plugin:2.0.0:aggregate-download-licenses",
        "-Dlicense.excludedScopes=#{@ignored_groups.to_a.join(',')}",
        "-Dorg.slf4j.simpleLogger.log.org.codehaus.mojo.license=debug",
        ENV.fetch('MAVEN_CLI_OPTS', '-DskipTests')
      ].join(' ')
    end

    def resource_files
      Pathname.glob(project_path.join('**', 'target', 'generated-resources', 'licenses.xml'))
    end

    def map_from(xml)
      ::License::Management.logger.debug(xml)
      XmlSimple
        .xml_in(xml, XML_PARSE_OPTIONS)['dependencies']
        .map { |dependency| MavenPackage.new(dependency) }
    end
  end
end
